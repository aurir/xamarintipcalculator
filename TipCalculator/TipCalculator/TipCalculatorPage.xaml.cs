﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace TipCalculator
{
    public partial class TipCalculatorPage : ContentPage
    {
        #region Calculator Values and Properties

        public double _percent = .2;
        public double _tip = 0;
        public double _amount = 0;

        public string Amount
        {
            get
            {
                return _amount.ToString();
            }
            set
            {
                double.TryParse(value, out _amount);
            }
        }

        public string Tip
        {
            get
            {
                return string.Format("{0:c}", _amount * _percent);
            }
        }

        public string Percent
        {
            get
            {
                return _percent.ToString();
            }
            set
            {
                double.TryParse(value, out _percent);
            }
        }

        #endregion

        public TipCalculatorPage()
        {
            InitializeComponent();
            BindingContext = this;
        }

        public void btnCalc_Clicked(object o, EventArgs a)
        {
            Amount = tbxAmount.Text.Trim();
            Percent = tbxPercent.Text.Trim();
            lblTip.Text = Tip;
        }
    }
}
