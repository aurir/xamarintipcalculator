﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TipCalculator
{
    public class Calculator
    {
        private double _tip = .2;

        public double Amount { get; set; }

        public double Tip { get { return Amount * _tip; } set { _tip = value; } }
    }
}
